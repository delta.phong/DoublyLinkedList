using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DoublyLinkedList_QuangPhu
{
    class Node
    {
        public int num;
        // tham chiếu đến Node trước
        public Node Prev;
        // tham chiếu đến Node sau
        public Node Next;

        // khởi tạo một Node rỗng, không có giá trị, không tham chiếu đến Node khác
        public Node()
        {
            num = 0;
            Prev = null;
            Next = null;
        }

        //khởi tạo một Node với nội dung chứa trong tham số x,không tham chiếu đến Node khác
        public Node(int x)
        {
            num = x;
            Prev = null;
            Next = null;
        }
    }
}
