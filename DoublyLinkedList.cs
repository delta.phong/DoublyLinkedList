using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DoublyLinkedList_QuangPhu
{
    class DoublyLinkedList
    {
        Node header;

        public DoublyLinkedList()
        {
            header = null;
        }

        /// <summary>
        /// ADD Node mới vào đầu danh sách
        /// </summary>
        /// <param name="newItem">giá trị Node mới</param>
        public void AddFirst(int newItem)
        {
            Node newNode = new Node(newItem);
            //nối Node mới vào đầu danh sách
            if (header == null) header = newNode;
            else
            {
                newNode.Next = header;
                header.Prev = newNode;
                header = newNode;
            }
        }

        /// <summary>
        /// AddLast  ---  ADD Node mới vào cuối danh sách
        /// </summary>
        /// <param name="newItem">giá trị Node mới</param>
        public void AddLast(int newItem)
        {
            Node newNode = new Node(newItem);
            //nếu List rỗng thì thêm vào đầu danh sách
            if (header == null) header = newNode;
            else
            {
                //xác định Node cuối
                Node end = header;
                while (end.Next != null) end = end.Next; 
                //nối Node mới vào cuối danh sách
                end.Next = newNode;
                newNode.Prev = end;
            }
        }

        /// <summary>
        /// SEARCH ---  Tìm Node đầu tiên có chứa giá trị k, trả về null nếu không tìm thấy
        /// </summary>
        /// <param name="k">giá trị Node cần tìm</param>
        /// <returns>Node</returns>
        public Node Search(int k)
        {
            Node current = new Node();
            current = header;
            while ((current != null) && (current.num != k))
                current = current.Next;
            return current;
        }
        
        /// <summary>
        /// ADDANY ---  Chèn Node mới vào vị trí Node đầu tiên có giá trị k
        /// </summary>
        /// <param name="newItem">giá trị Node mới</param>
        /// <param name="k">giá trị Node ở vị trí sẽ chèn</param>
        public void AddAny(int newItem, int k)
        {
            Node newNode = new Node(newItem);
            Node current = Search(k);
            if (current != null)
            {
                if (header == current) AddFirst(newItem);
                //nếu current là header thì AddFirst
                else
                {
                    // nối Node mới vào danh sách
                    newNode.Prev = current.Prev;
                    current.Prev.Next = newNode;
                    current.Prev = newNode;
                    newNode.Next = current;
                }
            }
        }

        /// <summary>
        /// REMOVE ---  Xóa Node đầu tiên có chứa giá trị k
        /// </summary>
        /// <param name="Item">giá trị muốn xóa</param>
        public void Remove(int Item)
        {
            Node current = Search(Item);
            if (current != null)//tìm thấy Node chứa Item 
            {
                if (current == header)
                {
                    header = header.Next; //nếu current là header thì chỉ định lại header
                    current = null;
                }
                else
                {
                    current.Prev.Next = current.Next; //nối Node trước current vào phần tử sau current
                    if (current.Next != null) current.Next.Prev = current.Prev;
                    //nếu current không phải là Node cuối, nối Prev của Node sau current vào Node trước current
                    current = null;
                }
            }
                
        }

        /// <summary>
        /// REMOVEALL  ---  Xóa danh sách
        /// </summary>
        public void RemoveAll()
        {
            header = null;
        }

        /// <summary>
        /// PRINT  ---  Trả về chuỗi các giá trị trong danh sách
        /// </summary>
        /// <returns>string</returns>
        public string Print()
        {
            string s = "";
            Node current = new Node();
            current = header;
            while (current != null)
            {
                s += current.num.ToString() + "\t";
                current = current.Next;
            }
            return s;
        }

        /// <summary>
        /// COUNT - Đếm số phần tử trong danh sách
        /// </summary>
        /// <returns></returns>
        public int Count()
        {
            Node current = new Node();
            current = header;
            int count = 0;
            while (current != null)
            {
                current = current.Next;
                count++;
            }
            return count;
        }
    } 
}
