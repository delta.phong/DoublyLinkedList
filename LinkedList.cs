using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExampleAtClass_20170912_BIT41
{
    class LinkedList
    {
        Node header;
        Boolean flag;

        public LinkedList()
        {
            header = null;
        }

        //AddFirst --- used to add a Node to the head of list.
        //Para: s 
        //Result: true/false
        public Boolean AddFirst(int s)
        {
            Boolean r = true;

            Node p = new Node(s);

            if (header == null)
                header = p;
            else
            {
                p.link = header;
                header = p;
            }
            return r;
        }
        //AddLast --- used to add a Node to the head of list.
        //Para: s 
        //Result: true/false
        public Boolean AddLast(int s)
        {
            Boolean r = true;
            Node p = new Node(s);
            Node q = header;
            //Duyet danh sach
            if (q != null)
            {
                while (q.link != null) q = q.link;
                q.link = p;
            }
            else
                header = p;
            return r;
        }

        //Search --- Tìm node chứa k trả về node cha của Node chứa k
        //Para: k
        //Result: null; Node cha của node chứa k; 
        //              nếu là node đầu thì Node cha cũng chính nó
        public Node Search(int k)
        {
            Node p = header; //node cha
            Node q = header; //node chứa k (nếu tìm thấy)
            flag = false;
            while ((q!=null) && (q.num != k))
            {
                p = q;
                q = q.link;
            }
            if (q == null) p = null;
            else if (header == q) flag = true; //xem k có phải header không

            return p;
        }

        //Count --- the total of Node in LinkedList
        //Para: none
        //Result: total of Node
        public int Count()
        {
            int count = 0;
            Node p = header;
            while (p!=null)
            {
                count++;
                p = p.link;
            }
            return count;
        }

        //Print -- used to show the list
        //Para: none
        //Result: string 
        public string Print()
        {
            string s = "";
            Node p = header;
            while (p!=null)
            {
                s += "," + p.num.ToString();
                p = p.link;
            }
            return s.Substring(1);
        }


        // -----------------------------------------------------------------------------------
        //PHẦN BÀI LÀM

        // *Thêm biến flag vào hàm search để xác định Node chứa k có phải header không. (Phía trên)

        /// <summary>
        /// ADDANY  ---  Thêm node mới vào trước Node đầu tiên có giá trị k 
        /// </summary>
        /// <param name="number">Giá trị Node mới</param>
        /// <param name="k">Giá trị Node ở vị trí muốn thêm</param>
        public void AddAny(int number, int k)
        {
            Node current = new Node();
            Node newNode = new Node(number);
            current = Search(k);
            if (current != null)
            {
                if (flag == true) AddFirst(number); 
                //nếu current là header thì AddFirst 
                else
                {
                    newNode.link = current.link;
                    current.link = newNode;
                }
            }
        }

        /// <summary>
        /// REMOVEALL  ---  Xóa Node đầu tiên có giá trị k
        /// </summary>
        /// <param name="k">Giá trị Node muốn xóa</param>
        public void Remove(int k)
        {
            Node current = Search(k);
            if (current != null)
            {
                if (flag == true)
                {
                    header = header.link;
                    current = null;
                }
                //nếu k là header thì chỉ header vào Node sau
                else
                {
                    Node remove = current.link;
                    current.link = current.link.link;
                    remove = null;
                }
                //ngược lại thì chỉ Node cha vào Node sau k rồi xóa Node k
            }
        }

        /// <summary>
        /// REMOVEALL Xóa danh sách
        /// </summary>
        public void RemoveAll()
        {
            header = null;
        }
    }
}
